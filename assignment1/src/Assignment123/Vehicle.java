package Assignment123;

import java.util.Objects;
import java.util.Scanner;

public class Vehicle {
public static void main(String[]args) {

		Scanner in = new Scanner(System.in);
        System.out.println("Enter the type of vehicle bike or car : ");
        String vehicle = in.nextLine();
        System.out.println("Enter the amount of petrol: ");
        int amt = in.nextInt();
        result1(amt,vehicle);
    }
    public static void result1(int amount, String vehicle)
    {
    	  int litres = amount/115;
          int distance=0;
          if(vehicle.equalsIgnoreCase("car"))
          {
              distance = 8*litres;
              }
          else if(vehicle.equalsIgnoreCase("bike"))
          {
              distance = 20*litres;
              }
          else
          {
              System.out.println("Enter valid vehicle type - (bike/car)");
              return;
              }
          System.out.println("Litres of petrol the customer gets : "+litres);
          System.out.println("Total distance customer can travel : "+distance);
          }
    }
